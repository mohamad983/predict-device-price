package com.test.project.service;

import com.test.project.entity.Device;
import com.test.project.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    public Device getDeviceById(Long id) {
        return deviceRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Device not found"));
    }

    @Transactional
    public Device addDevice(Device device) {
        return deviceRepository.save(device);
    }


    @Transactional
    public Device predictPrice(Long deviceId) {
        RestTemplate restTemplate = new RestTemplate();
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new RuntimeException("Device not found"));

        // Create headers
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        // Convert device object to JSON string
        ObjectMapper objectMapper = new ObjectMapper();
        String deviceJson;
        try {
            deviceJson = objectMapper.writeValueAsString(device);
        } catch (Exception e) {
            throw new RuntimeException("Error converting device to JSON", e);
        }

        // Create request entity
        HttpEntity<String> requestEntity = new HttpEntity<>(deviceJson, headers);

        // TODO: Replace with your Python API endpoint URL
        String apiUrl = "http://localhost:5007/api/predict";

        // Send POST request
        ResponseEntity<Device> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, requestEntity, Device.class);

        Device predictedDevice = responseEntity.getBody();

        device.setPredictedPrice(predictedDevice.getPredictedPrice());
        deviceRepository.save(device);

        return device;
    }

    private String convertDeviceToSpecs(Device device) {
        // Convert device fields to a string format for the Python API
        return String.format("%d,%d,%b,%.1f,%b,%d,%b,%d,%.1f,%d,%d,%d,%d,%d,%d,%d,%d,%b,%b,%b",
                device.getBatteryPower(), device.isBlue() ? 1 : 0, device.getClockSpeed(), device.isDualSim() ? 1 : 0,
                device.getFc(), device.isFourG() ? 1 : 0, device.getIntMemory(), device.getMDep(), device.getMobileWt(),
                device.getNCores(), device.getPc(), device.getPxHeight(), device.getPxWidth(), device.getRam(),
                device.getScH(), device.getScW(), device.getTalkTime(), device.isThreeG() ? 1 : 0,
                device.isTouchScreen() ? 1 : 0, device.isWifi() ? 1 : 0);
    }
}
