

# Contents:
- [AI Server](#ai-server)
    - [Requirements](#requirements)
    - [Usage & Run](#usage--run)
    - [Storage](#storage)
    - [AI Routes](#routes)
- [Spring boot Server](#spring-boot-server)
    - [Requirements](#spring-requirments)
    - [Usage & Run](#run-spring-server)
    - [Spring boot Routes](#spring-boot-routes)
- [End-points postman collection](#end-points-collection)    


# AI Server

## requirements

```
    - Python >= 3.10.12
    - Install libraries that listed in attached requiremnts.txt
```

# Usage $ run

###### Create separate env, install the required packages.

```
- pip install -r requirements.txt
- run AI server server/main.py
```

# Storage

## Training & Testing Data
install the training and testing data at the listed pathes
- ./data/train.csv
- ./data/test.csv

## Model Storage
install the training and testing data at the listed pathes
- ./server/data/lr_predictor

# Routes

## Predict device price using AI end-point:

```
 Url: localhost:5007/api/predict
 Method : POST
 Payload: {
    "batteryPower":842,
    "blue":0,
    "clockSpeed":2.2,
    "dualSim":0,
    "fc":0,
    "fourG":0,
    "intMemory":7,
    "mDep":0.6,
    "mobileWt":188,
    "nCores":2,
    "pc":2,
    "pxHeight":20,
    "pxWidth":756,
    "ram":2549,
    "scH":9,
    "scW":7,
    "talkTime":19,
    "threeG":0,
    "touchScreen":0,
    "wifi":1,
    "priceRange":1
}

```

###### success response

```
{
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "predictedPrice": 1,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
},200
```
<img src="sceenshots/AI-server-predict-price.png" alt="image" width="400" height="auto">

----


# Spring Boot Server

## Spring Requirments

```
    - Java 17
    - MySql Database 
```
## Run Spring Server
```
    - run the server localhost:8090
```

### Spring Boot Routes

#### Get all devices

```
 Url: localhost:8090/api/devices
 Method : GET
```

###### success response

```
[{
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "predictedPrice": 1,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
}],200
```
<img src="sceenshots/get-all-devices.png" alt="image" width="400" height="auto">

#### Get device by id

```
 Url: localhost:8090/api/devices/{device-id}
 Method : GET
```

###### success response

```
{
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "predictedPrice": 1,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
},200
```
<img src="sceenshots/get-device-by-id.png" alt="image" width="400" height="auto">

#### Add device

```
 Url: localhost:8090/api/devices
 Method : POST
 Payload: {
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
}
```

###### success response

```
{
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
},200
```
<img src="sceenshots/add-device.png" alt="image" width="400" height="auto">

#### Predict device price

```
 Url: localhost:8090/api/devices/predict/{device-id}
 Method : POST
```

###### success response

```
{
    "batteryPower": 842.0,
    "blue": 0.0,
    "clockSpeed": 2.2,
    "dualSim": 0.0,
    "fc": 0.0,
    "fourG": 0.0,
    "intMemory": 7.0,
    "mDep": 0.6,
    "mobileWt": 188.0,
    "nCores": 2.0,
    "pc": 2.0,
    "predictedPrice": 1,
    "priceRange": 1.0,
    "pxHeight": 20.0,
    "pxWidth": 756.0,
    "ram": 2549.0,
    "scH": 9.0,
    "scW": 7.0,
    "talkTime": 19.0,
    "threeG": 0.0,
    "touchScreen": 0.0,
    "wifi": 1.0
},200
```
<img src="sceenshots/add-device.png" alt="image" width="400" height="auto">


### End-Points Collection

```
you can use endpoints collection that attached in the root folder at path: 
./Mobile price prediction.postman_collection.json
```