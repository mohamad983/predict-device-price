def num_device_attributes():
    return ["battery_power","fc","clock_speed","int_memory","m_dep","mobile_wt","pc","px_height","px_width","ram","sc_h","sc_w","talk_time"]
def cat_device_attributes():
    return ["n_cores","blue","dual_sim","four_g","three_g","touch_screen","wifi"]
def device_attributes():
    return num_device_attributes()+cat_device_attributes()