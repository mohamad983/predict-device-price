from routes.app_routes import app_routes
from flask import Flask
from flask_cors import CORS
from service.device_price_service import DevicePricingService

app = Flask(__name__)
app.debug = True
CORS(app)
app_routes(app)

if __name__ == "__main__":
    app.run(port=5007)
