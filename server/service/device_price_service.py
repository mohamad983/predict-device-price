import pandas as pd
import pickle
import os.path
class DevicePricingService():
    model=None
    normalizer=None
    _instance = None
    _initialized = False
    
    def __new__(self):
        if self._instance is None:
            self._instance = super(DevicePricingService, self).__new__(self)
        return self._instance    

    def __init__(self):
        if self._initialized:
            return
        self._initialized = True
        self.model=self.load_model(path=os.path.dirname(__file__) + '/../data/lr_predictor')
        self.normalizer=self.load_model(path=os.path.dirname(__file__) + '/../data/pipeline_normalizer')
        
    def predict(self,input_case:pd.DataFrame):
        transformed_df=self.normalizer.transform(input_case)
        predicted=self.model.predict(transformed_df)
        return predicted
    
    def load_model(self,path):
        with open(path,"rb")as file:
            model=pickle.load(file)
            return model
        
    def invers_transform_target(self,predicted:list):
        prdicted_class=[]
        for predicted_traget in predicted:
            match predicted_traget:
                case 0:
                    prdicted_class.append("low")
                case 1:
                    prdicted_class.append("medium")
                case 2:
                    prdicted_class.append("high")
                case 3:
                    prdicted_class.append("very high")
                case _:
                    prdicted_class.append("unknown")
        return prdicted_class    
                    
