from functools import wraps
from utils.parse_requests import pares_post_request
from flask import request, make_response, jsonify


def predict_device_price_payload(next_to):
    @wraps(next_to)
    def wrapper(*args, **kwargs):
        data = pares_post_request(request=request)
        print(data)
        batteryPower = data.get("batteryPower")
        blue=data.get("blue")
        clockSpeed=data.get("clockSpeed")
        dualSim=data.get("dualSim")
        fc=data.get("fc")
        fourG=data.get("fourG")
        intMemory=data.get("intMemory")
        mDep=data.get("mdep")
        mobileWt=data.get("mobileWt")
        nCores=data.get("ncores")
        pc=data.get("pc")
        pxHeight=data.get("pxHeight")
        pxWidth=data.get("pxWidth")
        ram=data.get("ram")
        scH=data.get("scH")
        scW=data.get("scW")
        talkTime=data.get("talkTime")
        threeG=data.get("threeG")
        touchScreen=data.get("touchScreen")
        wifi=data.get("wifi")
        priceRange=data.get("priceRange")
       #if not :
        #    return make_response(jsonify({
         #       "message": "question is required",
          #      "success": False
           # })), 406
        return next_to({"battery_power":[batteryPower],"fc":[fc],"clock_speed":[clockSpeed],"int_memory":[intMemory],"m_dep":[mDep],"mobile_wt":[mobileWt],"pc":[pc],"px_height":[pxHeight],"px_width":[pxWidth],"ram":[ram],"sc_h":[scH],"sc_w":[scW],"talk_time":[talkTime],"n_cores":[nCores],
                        "blue":[blue],"dual_sim":[dualSim],"four_g":[fourG],"three_g":[threeG],"touch_screen":[touchScreen],
                        "wifi":[wifi],"price_range":[priceRange]}, *args, **kwargs)

    return wrapper
