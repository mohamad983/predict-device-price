from middleware.predict_device_price_payload import predict_device_price_payload
from flask import make_response, jsonify
import pandas as pd
from service.device_price_service import DevicePricingService
from utils.constant.device_attributes import device_attributes
@predict_device_price_payload
def predict_device_price(df_dic: dict):
    try:
        df=pd.DataFrame.from_dict(df_dic)  
        print(df_dic)      
        input_case=df[device_attributes()].copy()
        device_price_service=DevicePricingService()
        predicted_codes=device_price_service.predict(input_case=input_case)
        print("\n---> predicted_classes: ",device_price_service.invers_transform_target(predicted=predicted_codes),"\n")
        prediction_res=[]
        df=df.reset_index()
        for index, row in df.iterrows():
            prediction_res.append(
                {"batteryPower":row["battery_power"],
                 "fc":row["fc"],"clockSpeed":row["clock_speed"],
                 "intMemory":row["int_memory"],"mdep":row["m_dep"],"mobileWt":row["mobile_wt"],"pc":row["pc"],"pxHeight":row["px_height"]
                 ,"pxWidth":row["px_width"],"ram":row["ram"],
                 "scH":row["sc_h"],"scW":row["sc_w"],"talkTime":row["talk_time"],
                 "ncores":row["n_cores"],"blue":row["blue"],
                 "dualSim":row["dual_sim"],"fourG":row["four_g"],"threeG":row["three_g"],
                 "touchScreen":row["touch_screen"],"wifi":row["wifi"],
                 "priceRange":row["price_range"],
                 "predictedPrice":int(predicted_codes[index])}
            )
        
        return make_response(jsonify(prediction_res[0] if prediction_res else {})), 200
    except Exception as e:
        print(e)
        return make_response(jsonify({
            "message": "A problem occurred during the process !!!",
            "success": False,
        })), 500
