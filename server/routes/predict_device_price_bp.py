from flask import Blueprint

from controller.predict_device_pricce_ctr import predict_device_price

predict_price_bp = Blueprint('predict_device_price_bp', __name__)

predict_price_bp.route('predict', methods=['POST'])(predict_device_price)