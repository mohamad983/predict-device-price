from routes.predict_device_price_bp import predict_price_bp


def app_routes(app):
    app.register_blueprint(predict_price_bp, url_prefix='/api')

